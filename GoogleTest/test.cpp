#include "pch.h"
#include "../RectanglesOverlap/point.cpp"
#include "../RectanglesOverlap/rectangle.cpp"

namespace
{
	const Rectangle r1 = Rectangle({ -2, -2 }, { 2, 2 });
}

//class RectangleAreaTestSuite : public testing::Test
//{
//	const Rectangle r = Rectangle({ -1, -1 }, { 1, 1 });
//};
//
//TEST_F(RectangleAreaTestSuite, nazwa)
//{
//
//}

TEST(PointConstructorTest, ConstructorWithParameters)
{
	Point p1 = Point{ -1, 1 };
	EXPECT_EQ(p1.x, -1);
	EXPECT_EQ(p1.y, 1);
}

TEST(RectangleConstructorTest, ConstructorWithParameters)
{
	EXPECT_EQ(r1.getBottomLeft().x, -2);
	EXPECT_EQ(r1.getBottomLeft().y, -2);

	EXPECT_EQ(r1.getTopLeft().x, -2);
	EXPECT_EQ(r1.getTopLeft().y, 2);

	EXPECT_EQ(r1.getTopRight().x, 2);
	EXPECT_EQ(r1.getTopRight().y, 2);

	EXPECT_EQ(r1.getBottomRight().x, 2);
	EXPECT_EQ(r1.getBottomRight().y, -2);
}

TEST(RectangleAreaTest, nonZeroArea)
{
	Rectangle r2({ -1, -1 }, { 1, 1 });
	Rectangle r3({ 1, 1 }, { 3, 3 });
	Rectangle r4({ -3, -3 }, { -1, -1 });

	EXPECT_EQ(r2.area(), 4);
	EXPECT_EQ(r3.area(), 4);
	EXPECT_EQ(r4.area(), 4);
}

TEST(RectangleNotOverlappingTest, LeftRight)
{
	Rectangle r2({ 3, 0 }, { 4, 4 });

	EXPECT_FALSE(r1.isOverlapping(r2));
	EXPECT_FALSE(r2.isOverlapping(r1));
}

TEST(RectangleNotOverlappingTest, UpDown)
{
	Rectangle r3({ 0, 3 }, { 4, 4 });

	EXPECT_FALSE(r1.isOverlapping(r3));
	EXPECT_FALSE(r3.isOverlapping(r1));
}

TEST(RectangleNotOverlappingTest, Diagonal)
{
	Rectangle r4({ 3, 3 }, { 4, 4 });

	EXPECT_FALSE(r1.isOverlapping(r4));
	EXPECT_FALSE(r4.isOverlapping(r1));
}

TEST(RectangleNotOverlappingTest, PointTouch)
{
	Rectangle r5({ 2, 2 }, { 4, 4 });

	EXPECT_FALSE(r1.isOverlapping(r5));
	EXPECT_FALSE(r5.isOverlapping(r1));
}

TEST(RectangleNotOverlappingTest, SideTouch)
{
	Rectangle r6({ 2, -1 }, { 3, 1 });

	EXPECT_FALSE(r1.isOverlapping(r6));
	EXPECT_FALSE(r6.isOverlapping(r1));
}

TEST(RectangleOverlappingTest, OneInAnother)
{
	Rectangle r2({ -1, -1 }, { 1, 1 });

	EXPECT_TRUE(r1.isOverlapping(r2));
	EXPECT_TRUE(r2.isOverlapping(r1));
}

TEST(RectangleOverlappingTest, Corner)
{
	Rectangle r3({ 1, 1 }, { 3, 3 });

	EXPECT_TRUE(r1.isOverlapping(r3));
	EXPECT_TRUE(r3.isOverlapping(r1));
}

TEST(RectangleOverlappingTest, Side)
{
	Rectangle r4({ -1, -1 }, { 3, 1 });

	EXPECT_TRUE(r1.isOverlapping(r4));
	EXPECT_TRUE(r4.isOverlapping(r1));
}

TEST(RectangleOverlappingTest, Cross)
{
	Rectangle r5({ -1, -3 }, { 1, 3 });

	EXPECT_TRUE(r1.isOverlapping(r5));
	EXPECT_TRUE(r5.isOverlapping(r1));
}

TEST(RectangleOverlapAreaTest, OneInAnother)
{
	Rectangle r2({ -1, -1 }, { 1, 1 });

	EXPECT_EQ(r1.overlapArea(r2), 4);
	EXPECT_EQ(r2.overlapArea(r1), 4);
}

TEST(RectangleOverlapAreaTest, Corner)
{
	Rectangle r3({ 1, 1 }, { 3, 3 });

	EXPECT_EQ(r1.overlapArea(r3), 1);
	EXPECT_EQ(r3.overlapArea(r1), 1);
}

TEST(RectangleOverlapAreaTest, Side)
{
	Rectangle r4({ -1, -1 }, { 3, 1 });

	EXPECT_EQ(r1.overlapArea(r4), 6);
	EXPECT_EQ(r4.overlapArea(r1), 6);
}

TEST(RectangleOverlapAreaTest, Cross)
{
	Rectangle r5({ -1, -3 }, { 1, 3 });

	EXPECT_EQ(r1.overlapArea(r5), 8);
	EXPECT_EQ(r5.overlapArea(r1), 8);
}

TEST(RectangleOverlapAreaTest, NotOverlapping)
{
	Rectangle r6({ 3, 4 }, { 5, 6 });

	EXPECT_EQ(r1.overlapArea(r6), 0);
	EXPECT_EQ(r6.overlapArea(r1), 0);
}