#pragma once
#include "point.h"
#include <cmath>

class Rectangle
{
public:
	Rectangle(const Point bottomLeft, const Point topRight);
	int area() const;
	bool isOverlapping(const Rectangle & other) const;
	Rectangle overlap(const Rectangle & other) const;
	int overlapArea(const Rectangle & other) const;
	
	Point getBottomLeft() const;
	Point getTopRight() const;
	Point getTopLeft() const;
	Point getBottomRight() const;

private:
	Point bottomLeft;
	Point topLeft;
	Point topRight;
	Point bottomRight;
};