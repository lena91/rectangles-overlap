#include "rectangle.h"
#include <algorithm>
#include <stdexcept>

Rectangle::Rectangle(const Point bottomLeft, const Point topRight) 
	: bottomLeft{ bottomLeft }, topLeft{ bottomLeft.x, topRight.y }, topRight{ topRight }, bottomRight{ topRight.x, bottomLeft.y }
{
	if (bottomLeft.x >= topRight.x || bottomLeft.y >= topRight.y)
	{
		throw std::invalid_argument("Invalid coordinates");
	}
}

int Rectangle::area() const
{
	return std::abs((topRight.x - bottomLeft.x) * (topRight.y - bottomLeft.y));
}

bool Rectangle::isOverlapping(const Rectangle & other) const
{
	if (topRight.x <= other.getBottomLeft().x ||
		bottomLeft.x >= other.getTopRight().x ||
		topRight.y <= other.getBottomLeft().y ||
		bottomLeft.y >= other.getTopRight().y)
	{
		return false;
	}
	return true;
}

Rectangle Rectangle::overlap(const Rectangle & other) const
{
	int top = std::min(this->getTopRight().y, other.getTopRight().y);
	int bottom = std::max(this->getBottomLeft().y, other.getBottomLeft().y);
	int left = std::max(this->getBottomLeft().x, other.getBottomLeft().x);;
	int right = std::min(this->getTopRight().x, other.getTopRight().x);

	return Rectangle({ bottom, left }, { top, right });
}

int Rectangle::overlapArea(const Rectangle & other) const
{
	if (!isOverlapping(other))
	{
		return 0;
	}
	
	return overlap(other).area();
}

Point Rectangle::getBottomLeft() const
{
	return bottomLeft;
}

Point Rectangle::getTopRight() const
{
	return topRight;
}

Point Rectangle::getTopLeft() const
{
	return topLeft;
}

Point Rectangle::getBottomRight() const
{
	return bottomRight;
}